const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    entry: {
        'editor': [
            './simple_flask_cms/frontend/editor.scss',
            './simple_flask_cms/frontend/editor.js'
        ],
        'overview': [
            './simple_flask_cms/frontend/overview.js'
        ],
        'fragment': [
            './simple_flask_cms/frontend/fragment.js'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'simple_flask_cms/static/generated'),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css'
        })
    ],
    resolve: {
        extensions: ['.js', '.scss']
    }
};